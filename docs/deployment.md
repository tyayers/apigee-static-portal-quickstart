id: apigee-static-portal-quickstart-deployment
summary: This is a quickstart for deploying this Apigee Static Portal Quickstart with Firebase Auth on Google Cloud Run.
authors: Tyler Ayers

# Deploying Apigee Static Portal Quickstart

## Introduction

Duration: 5

In this lab we will deploy a simple static API portal for Apigee API management using Firebase Authentication for user identity management (but which could be changed to any 3rd party IDP system).  The portal will run as a service in Google Cloud Run.

Here is the architecture for the solution.

![Portal architecture](img/portal-architecture.png)

Here is the static developer portal that we want to deploy, which will give registered users an overview of available APIs, and let them subscribe and create keys for the APIs (self-service).  The actual APIs and keys are managed in Apigee, we are just deploying a static portal, securing that portal with Firebase Authentication, and demonstrating how this basic portal can offer API products to developers.

Positive
: The other [portal options in Apigee](https://docs.apigee.com/api-platform/publish/developer-portal) are much more powerful, with features like SSO and monetization available out of the box, and so are the recommended solution.  This is simply an example of how a custom static portal can work, if for whatever reasons a custom portal is required.

## Configure Firebase Authentication

Duration: 10

Firebase Authentication (also referred to as the Google Cloud Identity Platform) is a fast and secure way to manage user identities, either in an internal directory, or connected to other OpenID Connect identity services such as Google, Facebook, Microsoft, etc...

### Create a free Firebase project
- Go to the [Firebase Console](https://console.firebase.google.com), login with any consumer Google account, and click on **Create a project** to create a free Firebase project.

![Create a Firebase project](img/firebase-create-project.png)

- Enter a name for your project (for example `apigee-portal` and Firebase will make it unique)
- Turn Google Analytics off in the next dialog (not needed for this lab)
- Click **Continue** when the new project is created.

### Configure Authentication and download key

- Click on **Develop** and then **Authentication** in the upper-left corner

![Click on Develop Authentication](img/firebase-develop.png)

- Click on **Set up sign-in method** and turn Email/Password **on** in the next screen (do not turn **Email link(passwordless sign-in)** since it is not a focus of this lab).

![Enable email](img/firebase-enable-email.png)

- Now we will download the Firebase Service key so that we can verify the Firebase Authentication tokens in our apigee-portal backend service.  With this small step we have a completely authenticated frontend and backend communication, which effectively secures our web app using OAuth tokens from Firebase.  See [Firebase Verify ID Tokens](https://firebase.google.com/docs/auth/admin/verify-id-tokens) for more information.

- Click on the gear icon and then **Project settings**

![Project settings](img/firebase-properties.png)

- Click on **Service accounts**

![Service accounts](img/firebase-service-accounts.png)

- Click on **Generage new private key** to download a new key to your computer

![Private key](img/firebase-key.png)

- Rename the downloaded key to **firebasekey.json** and keep it for later in the lab.

![Private key renamed](img/firebase-key-renamed.png)

Congratulations!  You now have Firebase Authentication configured and ready to integrate with your new Apigee static portal.

## Deploy the Apigee Static Portal to Cloud Run

Duration: 15

Now we will clone and deploy the Apigee Static Portal project on [Google Cloud Run](https://cloud.google.com/run), which is a fast and easy way to host any container (including our portal) in the cloud.  You can get a free [Google Cloud](https://cloud.google.com) account with $300 to use in a year.  Cloud Run also includes 2 million free calls a month, which is more than enough for our simple portal.

You can do these steps on any computer with Docker and [gcloud](https://cloud.google.com/sdk/gcloud), however it is easiest in the [Google Cloud Shell](https://cloud.google.com/shell/docs) where everything is automatically configured and authenticated.

### Clone the Apigee Static Portal Quickstart repository

- If you are using **Google Cloud Shell** open the shell in the [Google Cloud console](https://console.cloud.google.com) by clicking the **Cloud Shell** button in the upper right corner.

![Start cloud shell](img/gcp-start-shell.png)


- Execute this command to clone the quickstart repository with the container source code:

`git clone https://gitlab.com/tyayers/apigee-static-portal-quickstart.git`

- cd into the newly created directory **apigee-static-portal-quickstart**

`cd apigee-static-portal-quickstart`

- Open the cloud editor so that we can browse and upload files.

![Open editor](img/gcp-open-editor.png)

- You should get the Cloud Shell Editor opened into a new tab, then right-click on the directory **apigee-portal** in the directory **apigee-static-portal-quickstart/services**, and click **Upload Files** to upload the **firebasekey.json** that we downloaded and renamed in the previous section.

![Upload key](img/gcp-upload-key.png)

- Your directory should look like this after uploading.

![Directory](img/gcp-directory.png)

### Build the Apigee Static Portal container image

- Now we can build our container to deploy to Cloud Run.  In the shell window, cd into the **apigee-static-portal-quickstart/services/apigee-portal** directory.

`cd apigee-static-portal-quickstart/services/apigee-portal`

- Run this command to get a list of your [GCP projects](https://cloud.google.com/resource-manager/docs/creating-managing-projects), and choose the project id of the project that you want to deploy your Cloud Run service in. 

`gcloud projects list`

- Set your chosen project id to the variable GCP_PROJECT with this command:

`GCP_PROJECT="PROJECT-ID"`

- Also set the default project in gcloud to our chosen project:

`gcloud config set project PROJECT-ID`

- Now let's build and push our container image to Google Cloud Container Registry.  Run these 3 commands to do that:

Positive
: You can change the region for the container registry in command 2 by changing **gcr.io** to **eu.gcr.io** for the EU or **asia.gcr.io** for Asia (see more info [here](https://cloud.google.com/container-registry/docs/pushing-and-pulling#pushing_an_image_to_a_registry)).

1. `docker build -t local/apigee-portal .`

2. `docker tag local/apigee-portal gcr.io/$GCP_PROJECT/apigee-portal`

3. `docker push eu.gcr.io/$GCP_PROJECT/apigee-portal`

You should see a positive message that the container was successfully pushed to the registry.

### Deploy the image to Cloud Run

In this section we will deploy our container to Cloud Run, and configure our service to integrate with the Apigee management API for developer and API product management features.

The apigee-portal service requires a service account in Apigee Edge to get the API products, and create the developers and key to let users access the APIs.  You can sign up for a free account at apigee.com, and create a service user under Admin->Users.

To deploy the apigee-portal service, you will need the name of your Apigee organization, the service account username, and the service account password.  These are given in the environment variables **APIGEE_ORG**, **APIGEE_USER**, and **APIGEE_PW**.  Set these variables with this command:

`APIGEE_ORG={your Apigee org name}`

`APIGEE_USER={your Apigee service account name}`

`APIGEE_PW={you Apigee service account password, url escaped}`

Run this account to deploy the configured service to Cloud Run:

`gcloud run deploy --image gcr.io/$GCP_PROJECT/apigee-portal --platform managed --set-env-vars=[APIGEE_ORG=$APIGEE_ORG,APIGEE_USER=$APIGEE_USER,APIGEE_PW=$APIGEE_PW]`

When asked for the **Service name** just accept the suggestion **apigee-portal**, and for the region selection choose a region that is closest to you and your container registry.  For the question **Allow unauthendicated invocations,** select Y since our service is secured through Firebase Authentication, and our portal should be available to anyone to visit.

After the deployment is complete, you should have a message with the URL of your app in the console.

![Cloud Run deployment](img/gcp-run-deploy.png)

Click the URL to open up the portal, you should see this landing page:

![Portal landing page](img/portal-landing.png)

Congratulations!  You now have deployed the static portal to Cloud Run, now we can create users and subscribe to apps in the next section.

## Test portal & create an app

Duration: 10

Now that we've deployed the app to Cloud Run, we can test our integration with Apigee by registering a developer and subscribing to some APIs.

### Register a new developer

Go to the Apigee Portal app, and click on **Register** to register a new developer.  Enter your name and an email address, and click submit.

![Portal register](img/portal-register.png)

After clicking Register, you will have to verify your email address, and then after reloading the main portal site you should be logged-in and see the API products view.

![Portal API products](img/portal-products.png)

When you click on Subscribe to an API, you should then be able to click on **Show Keys,** and see your keys that were created for the product, which are coming from Apigee.

![Portal API product keys](img/portal-keys.png)

In the Apigee console you can see that our developer was registered automatically through the apigee-portal service.

![Apigee developer](img/portal-developer.png)

And if you go to developer detail, you will see that we saved the Firebase Authentication Id as a custom attribute, and that the apps & keys the user has created are created and maintained in Apigee.

![Apigee developer](img/portal-developer-detail.png)

Congratulations!  You have successfully deployed an example static portal with Firebase Authentication