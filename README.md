# Apigee Static Portal Quickstart

**LIVE Demo:** [https://apigee-static-portal-h7pi7igbcq-ew.a.run.app](https://apigee-static-portal-h7pi7igbcq-ew.a.run.app)

**DEPLOYMENT Guide:** [https://tyayers.gitlab.io/apigee-static-portal-quickstart/#0](https://tyayers.gitlab.io/apigee-static-portal-quickstart/#0)

This is a quickstart project for testing a static Apigee Portal using Firebase Auth for user authentication and Apigee for API Management.

The project is implemented as a nodejs project that both offers the middleware to integrate with the Apigee Management API, and the static HTML content to serve the portal and integrate with the Firebase Auth for authentication.  The service is deployed on Google Cloud Run for easy hosting.

## Supported features

This project allows you to have a simple portal where users can register (using Firebase Auth), and after signing in, browse the published API Products (from Apigee) and subscribe to the products to create keys.  The users can also delete their subscriptions and keys, and then sign back out again, creating a harmonious cycle of creation and destruction in the API universe.

## Architecture

![Apigee static portal architecture](/docs/img/portal-architecture.png)

## Deployment Guide
If you want to deploy the project in your own environment with your own Firebase Auth and Apigee projects, simply follow this simple Cloud Lab [Deployment Guide](https://tyayers.gitlab.io/apigee-static-portal-quickstart/#0).

## Build
'gcloud builds submit --config ./cloudbuild.yaml ./services/apigee-portal'

## Customization
You easily clone, run and customize the project in any environment with nodejs.  After cloning the project, you can depoy the project with 3 easy steps.

1. Download your Firebase private key, rename it to **firebasekey.json**, and copy it into the **/services/apigee-portal** directory.
2. Create a file named **.env** also in the **/services/apigee-portal** directory with 3 environment variables defined:
 1. APIGEE_ORG=your org
 2. APIGEE_USER=your apigee service user email
 3. APIGEE_PW=your apigee service user password
3. Start the service with **node src** in the **/services/apigee-portal** directory.  The service is then available on port 8080.

### Backend
The backend is a simple express nodejs service, which can be easily extended and customized as needed.

### Frontend
The frontend is a simple Vue.js project with each part of the UI implemented as a component in the components directory.  No precompiling is necessary, just a static site with Vue.js frontend logic, can run anywhere.

## Screenshot
<center><img src="img/apigee-portal-products.png" /></center>

## Credits
* Nice MVP CSS styling: https://andybrewer.github.io/mvp/
