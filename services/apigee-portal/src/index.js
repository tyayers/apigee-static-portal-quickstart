"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var express = require('express');
var cors = require('cors');
var bodyParser = require('body-parser');
var axios = require('axios');
var admin = require('firebase-admin');
require('dotenv').config();
var yaml = require('js-yaml');
var fs = require('fs');
var apigee = require('./modules/apigee');
var apigeeService = new apigee.Apigee();
// Init app
var binaryData = Buffer.from(process.env.FIREBASE_KEY, "base64");
var serviceAccount = JSON.parse(binaryData.toString());
admin.initializeApp({
    credential: admin.credential.cert(serviceAccount)
});
var app = express();
app.use(express.static('public'));
app.use(bodyParser.json());
app.use(cors());
var config = yaml.safeLoad(fs.readFileSync('portal-config.yaml', 'utf8'));
var orgs = [process.env.APIGEE_ORG];
if (config) {
    for (var i = 0; i < config.portal.products.length; i++) {
        var product = config.portal.products[i];
        if (product.apigeeOrg != null && !orgs.includes(product.apigeeOrg)) {
            orgs.push(product.apigeeOrg);
        }
    }
}
// Middleware to validate token
app.use(function (req, res, next) {
    if (!req.headers.authorization) {
        return res.status(403).json({ error: 'No credentials sent!' });
    }
    else {
        var bearer = req.headers.authorization.replace(/^Bearer\s/, '');
        // idToken comes from the client app
        admin.auth().verifyIdToken(bearer).then(function (decodedToken) {
            var uid = decodedToken.uid;
            req.headers["email"] = decodedToken["email"];
            next();
            // ...
        }).catch(function (error) {
            console.log("token validation error! " + error);
            return res.status(403).json({ error: 'Invalid authorization token sent!' });
            // Handle error
        });
    }
});
// Creates a developer
app.post('/developers', function (req, res) {
    console.log("create developer request received");
    console.log("payload recieved: " + JSON.stringify(req.body, null, '\t'));
    var devEmail = req.body.email;
    var devFirstName = req.body.firstName;
    var devLastName = req.body.lastName;
    var firebaseId = req.body.firebaseId;
    apigeeService.getApigeeToken().then(function (token) {
        var devData = {
            email: devEmail,
            firstName: devFirstName,
            lastName: devLastName,
            userName: req.body.userName,
            attributes: [
                {
                    name: "identityProviderId",
                    value: firebaseId
                },
                {
                    name: "MINT_DEVELOPER_ADDRESS",
                    value: "{\"address1\":\"Dev One Address\",\"city\":\"Pleasanton\",\"country\":\"US\",\"isPrimary\":true,\"state\":\"CA\", \"zip\":\"94588\"}"
                },
                {
                    name: "MINT_DEVELOPER_LEGAL_NAME",
                    value: devFirstName + " " + devLastName
                },
                {
                    name: "MINT_DEVELOPER_TYPE",
                    value: "TRUSTED"
                }
            ]
        };
        apigeeService.createOrUpdateApigeeDev(token, devData, process.env.APIGEE_ORG).then(function (result) {
            admin.auth().setCustomUserClaims(firebaseId, { apigeeId: result.developerId }).then(function () {
                // The new custom claims will propagate to the user's ID token the
                // next time a new one is issued.
                console.log("firebase set claim successful, now returning to client..");
                // Now send info back to the client..
                res.status(201).send({ developerId: result.developerId });
            });
        }, function (error) {
            throw error;
        }).catch(function (error) {
            console.log("Error while creating developer.");
            console.log(error);
            res.status(500).send({ message: "Could not create developer." });
        });
    }, function (err) {
        return res.status(403).json({ error: 'Invalid authorization with backend system.' });
    });
});
// Gets a developer
app.get('/developers/:devId', function (req, res) {
    var devEmail = req.headers.email;
    var org = process.env.APIGEE_ORG;
    apigeeService.getApigeeToken().then(function (token) {
        apigeeService.getDev(token, devEmail, org).then(function (dev) {
            apigeeService.getDevApps(token, devEmail, org).then(function (apps) {
                if (apps.length > 0)
                    dev.devApps = apps;
                apigeeService.getDevRatePlans(token, devEmail, org).then(function (rates) {
                    dev.devRatePlans = rates;
                    apigeeService.getDevCreditAccounts(token, devEmail, org).then(function (accounts) {
                        if (accounts.length > 0)
                            dev.devCreditAccounts = accounts;
                        res.send(dev);
                    });
                });
            });
        });
    }).catch(function (error) {
        console.error("Error in get dev: " + JSON.stringify(error));
        res.status(500).json({ error: error.message });
    });
});
// Gets all of the API Products
app.get('/api-products', function (req, res) {
    var result = [];
    var productNames = [];
    if (config) {
        for (i = 0; i < config.portal.products.length; i++) {
            var product = config.portal.products[i];
            var entry = {
                name: product.apiProductName,
                displayName: product.displayName,
                description: product.description,
                image: product.image,
                specUrl: product.specUrl,
                status: "documentation"
            };
            if (product.apigeeOrg)
                entry.status = "subscribe";
            result.push(entry);
            productNames.push(product.apiProductName);
        }
    }
    apigeeService.getApigeeToken().then(function (token) {
        var config = {
            url: "https://api.enterprise.apigee.com/v1/organizations/" + process.env.APIGEE_ORG + "/apiproducts?expand=true",
            method: "get",
            headers: {
                Authorization: "Bearer " + token,
                "Content-Type": "application/json"
            }
        };
        axios(config).then(function (response) {
            var products = response.data;
            for (i = 0; i < products.apiProduct.length; i++) {
                if (productNames.length == 0) {
                    var api = {
                        name: products.apiProduct[i].name,
                        displayName: products.apiProduct[i].displayName,
                        description: products.apiProduct[i].description,
                        status: "subscribe",
                        specUrl: undefined
                    };
                    if (products.apiProduct[i].attributes.length > 0) {
                        for (var a = 0; a < products.apiProduct[i].attributes.length; a++) {
                            var attr = products.apiProduct[i].attributes[a];
                            if (attr.name == "specurl") {
                                api.specUrl = attr.value;
                            }
                        }
                    }
                    result.push(api);
                }
                else if (productNames.includes(products.apiProduct[i].name)) {
                    var api_1 = result.find(function (x) { return x.name === products.apiProduct[i].name; });
                    api_1.status = "subscribe";
                }
            }
            //console.log(result);
            res.send(result);
        }).catch(function (error) {
            console.log(error);
            res.status(500).send({ message: error });
        }).then(function () {
        });
    }, function (err) {
        console.log(err);
        res.status(500).send({ message: err });
    });
});
// Creates an app with keys
app.post('/developers/:devId/apps', function (req, res) {
    console.log("create app request received for developer " + req.headers.email);
    var devEmail = req.headers.email;
    var apiProduct = req.body.apiProduct;
    var org = process.env.APIGEE_ORG;
    if (config) {
        var api = config.portal.products.find(function (x) { return x.apiProductName === apiProduct; });
        if (api && api.apigeeOrg) {
            org = api.apigeeOrg;
        }
    }
    apigeeService.getApigeeToken().then(function (token) {
        apigeeService.createDevApp(token, devEmail, apiProduct, org).then(function (result) {
            res.status(201).send(result);
        }).catch(function (error) {
            res.status(500).send(error);
        });
    }, function (err) {
        res.status(500).send({ message: err });
    });
});
// Deletes an app
app.delete('/developers/:devId/apps/:appName', function (req, res) {
    console.log("delete app request received for developer " + req.headers.email + " and app " + req.params.appName);
    console.log("payload recieved: " + JSON.stringify(req.body));
    var devEmail = req.headers.email;
    var appName = req.params.appName;
    var org = process.env.APIGEE_ORG;
    if (config) {
        var api = config.portal.products.find(function (x) { return x.apiProductName === appName; });
        if (api && api.apigeeOrg) {
            org = api.apigeeOrg;
        }
    }
    apigeeService.getApigeeToken().then(function (token) {
        var config = {
            url: "https://api.enterprise.apigee.com/v1/organizations/" + org + "/developers/" + devEmail + "/apps/" + appName,
            method: "delete",
            headers: {
                Authorization: "Bearer " + token,
                "Content-Type": "application/json"
            }
        };
        axios(config).then(function (response) {
            console.log("delete app status " + response.status);
            if (response.status == 200) {
                console.log("Successfully deleted app");
                // Now send info back to the client..
                res.status(200).send();
            }
            else
                res.status(500).send(response.data);
        }).catch(function (error) {
            console.log(error);
            res.status(500).send({ message: error });
        }).then(function () {
        });
    }, function (err) {
        res.status(500).send({ message: err });
    });
});
// Gets all the developer's apps
app.get('/developers/:devId/apps', function (req, res) {
    console.log("enter developer apps get.. + " + JSON.stringify(orgs, null, '\t'));
    apigeeService.getApigeeToken().then(function (token) {
        var promises = [];
        for (i = 0; i < orgs.length; i++) {
            var org = orgs[i];
            promises.push(apigeeService.getDevApps(token, req.headers.email, org));
        }
        Promise.all(promises).then(function (result) {
            var resultArray = [];
            for (i = 0; i < result.length; i++) {
                resultArray = resultArray.concat(result[i]);
            }
            res.status(200).send(resultArray);
        }).catch(function (error) {
            console.log(error);
            res.status(500).send({ message: error });
        });
    }, function (err) {
        res.status(500).send({ message: err });
    });
});
// Gets all the rate plans
app.get('/rateplans', function (req, res) {
    apigeeService.getApigeeToken().then(function (token) {
        var promises = [];
        for (i = 0; i < orgs.length; i++) {
            var org = orgs[i];
            promises.push(apigeeService.getRatePlans(token, org));
        }
        Promise.all(promises).then(function (result) {
            var resultArray = [];
            var finalResultArray = [];
            for (i = 0; i < result.length; i++) {
                resultArray = resultArray.concat(result[i]);
            }
            if (config.portal.rateplans && config.portal.rateplans.length > 0) {
                var _loop_1 = function (i_1) {
                    configuredRate = config.portal.rateplans.find(function (x) { return x.name == resultArray[i_1].name; });
                    if (!configuredRate) {
                        //if (!config.portal.rateplans.includes(resultArray[i].name)) {
                        resultArray.splice(i_1, 1);
                    }
                    else {
                        resultArray[i_1].image = configuredRate.image;
                        finalResultArray[config.portal.rateplans.indexOf(configuredRate)] = resultArray[i_1];
                    }
                };
                var configuredRate;
                for (var i_1 = resultArray.length - 1; i_1 >= 0; i_1--) {
                    _loop_1(i_1);
                }
            }
            else
                finalResultArray = resultArray;
            res.status(200).send(finalResultArray);
        }).catch(function (error) {
            console.log(error);
            res.status(500).send({ message: error });
        });
    }, function (err) {
        res.status(500).send({ message: err });
    });
});
// Purchases a rate plan
app.post('/rateplans/:planid', function (req, res) {
    //console.log("create app request received for developer " + req.headers.email);
    var devEmail = req.headers.email;
    var rateId = req.params.planid;
    ;
    var org = process.env.APIGEE_ORG;
    apigeeService.getApigeeToken().then(function (token) {
        apigeeService.createDevRateSub(token, devEmail, rateId, org).then(function (result) {
            res.status(201).send(result);
        }).catch(function (error) {
            res.status(500).send(error);
        });
    }, function (err) {
        res.status(500).send({ message: err });
    });
});
// Delete purchased rate plan
app.delete('/rateplans/:planid/:plansubid', function (req, res) {
    var devEmail = req.headers.email;
    var rateId = req.params.planid;
    var rateSubId = req.params.plansubid;
    var org = process.env.APIGEE_ORG;
    apigeeService.getApigeeToken().then(function (token) {
        apigeeService.deleteDevRateSub(token, devEmail, rateId, rateSubId, org).then(function (result) {
            res.status(200).send(result);
        }).catch(function (error) {
            res.status(500).send(error);
        });
    }, function (err) {
        res.status(500).send({ message: err });
    });
});
// Purchases credits
app.post('/rateplans/:currency/credits', function (req, res) {
    //console.log("create app request received for developer " + req.headers.email);
    var devEmail = req.headers.email;
    var currency = req.params.currency;
    var amount = req.body.amount;
    var org = process.env.APIGEE_ORG;
    apigeeService.getApigeeToken().then(function (token) {
        apigeeService.addDevCredits(token, devEmail, currency, amount, org).then(function (result) {
            res.status(201).send(result);
        }).catch(function (error) {
            res.status(500).send(error);
        });
    }, function (err) {
        res.status(500).send({ message: err });
    });
});
app.listen(8080, function () { return console.log('apigee-portal service started!'); });
