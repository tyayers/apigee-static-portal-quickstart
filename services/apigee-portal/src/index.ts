export {};

const express = require('express');
var cors = require('cors');
const bodyParser = require('body-parser');
let axios = require('axios');
const admin = require('firebase-admin');
require('dotenv').config();
const yaml = require('js-yaml');
const fs   = require('fs');
let apigee = require('./modules/apigee');
let apigeeService: ApigeeInterface = new apigee.Apigee();

// Init app
var binaryData: ArrayBuffer = Buffer.from(process.env.FIREBASE_KEY as string, "base64");
var serviceAccount = JSON.parse(binaryData.toString());

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount)
});
const app = express();
app.use(express.static('public'));
app.use(bodyParser.json());
app.use(cors());

const config = yaml.safeLoad(fs.readFileSync('portal-config.yaml', 'utf8'));
const orgs: string[] = [ process.env.APIGEE_ORG as string ];

if (config) {
  for(var i=0; i<config.portal.products.length; i++) {
    var product = config.portal.products[i];
    if (product.apigeeOrg != null && !orgs.includes(product.apigeeOrg)) {
      orgs.push(product.apigeeOrg);
    }
  }
}

// Middleware to validate token
app.use(function(req: any, res: any, next: any) {
	if (!req.headers.authorization) {
		return res.status(403).json({ error: 'No credentials sent!' });
	}
	else {
		var bearer = req.headers.authorization.replace(/^Bearer\s/, '');
		// idToken comes from the client app
		admin.auth().verifyIdToken(bearer).then(function(decodedToken: any) {
			let uid = decodedToken.uid;
			req.headers["email"] = decodedToken["email"];
			next();
			// ...
		}).catch(function(error: any) {
			console.log("token validation error! " + error);
			return res.status(403).json({ error: 'Invalid authorization token sent!' });
			// Handle error
		});
	}
});

// Creates a developer
app.post('/developers', (req: any, res: any) => {

	console.log("create developer request received");
	console.log("payload recieved: " + JSON.stringify(req.body, null, '\t'));

	var devEmail = req.body.email;
	var devFirstName = req.body.firstName;
	var devLastName = req.body.lastName;
	var firebaseId = req.body.firebaseId;

	apigeeService.getApigeeToken().then(function(token: string) {

        var devData: apigeeDeveloper = {
          email: devEmail,
          firstName: devFirstName,
          lastName: devLastName,
          userName: req.body.userName,
          attributes: [
            {
              name: "identityProviderId",
              value: firebaseId
            },
            {
              name: "MINT_DEVELOPER_ADDRESS",
              value: "{\"address1\":\"Dev One Address\",\"city\":\"Pleasanton\",\"country\":\"US\",\"isPrimary\":true,\"state\":\"CA\", \"zip\":\"94588\"}"
            },
            {
              name: "MINT_DEVELOPER_LEGAL_NAME",
              value: devFirstName + " " + devLastName
            },
            {
             name: "MINT_DEVELOPER_TYPE",
             value: "TRUSTED"
            }                              
          ]
        };

        apigeeService.createOrUpdateApigeeDev(token, devData, process.env.APIGEE_ORG as string).then(function(result: any) {
            admin.auth().setCustomUserClaims(firebaseId, {apigeeId: result.developerId}).then(() => {
                // The new custom claims will propagate to the user's ID token the
                // next time a new one is issued.
                console.log("firebase set claim successful, now returning to client..");
                // Now send info back to the client..
                
                res.status(201).send({developerId: result.developerId});
            });
        }, function(error: any) {
            throw error;
        }).catch(function(error: any) {
            console.log("Error while creating developer.");
            console.log(error);
			      res.status(500).send({message: "Could not create developer."});
        });
	}, function(err: any) {
		return res.status(403).json({ error: 'Invalid authorization with backend system.' });
	});
});

// Gets a developer
app.get('/developers/:devId', (req: any, res: any) => {
  var devEmail = req.headers.email;
  var org = process.env.APIGEE_ORG as string;

  apigeeService.getApigeeToken().then(function(token: string) {
    apigeeService.getDev(token, devEmail, org).then(function(dev: developer) {
      apigeeService.getDevApps(token, devEmail, org).then(function(apps: app[]) {
        if (apps.length > 0)
          dev.devApps = apps;

        apigeeService.getDevRatePlans(token, devEmail, org).then(function(rates: devRate[]) {
          dev.devRatePlans = rates;

          apigeeService.getDevCreditAccounts(token, devEmail, org).then(function(accounts: devCreditAccount[]) {
            if (accounts.length > 0)
              dev.devCreditAccounts = accounts;

              res.send(dev);
          });
        })
      });
    });
  }).catch(function (error) {
    console.error("Error in get dev: " + JSON.stringify(error));
    res.status(500).json({error: error.message});
  });
})

// Gets all of the API Products
app.get('/api-products', (req: any, res: any) => {

  var result: apiProduct[] = [];
  var productNames: string[] = [];

  if (config) {
    for(i=0; i<config.portal.products.length; i++) {
      var product = config.portal.products[i];
      var entry = {
        name: product.apiProductName,
        displayName: product.displayName,
        description: product.description,
        image: product.image,
        specUrl: product.specUrl,
        status: "documentation"
      };

      if (product.apigeeOrg)
        entry.status = "subscribe";

      result.push(entry);

      productNames.push(product.apiProductName);
    }
  }

	apigeeService.getApigeeToken().then(function(token: string) {
		var config = {
			url: "https://api.enterprise.apigee.com/v1/organizations/" + process.env.APIGEE_ORG + "/apiproducts?expand=true",
			method: "get",
			headers: {
				Authorization: "Bearer " + token,
				"Content-Type": "application/json"
			}
		};

		axios(config).then(function (response: response) {

      let products: apigeeApiProductResponse = response.data as apigeeApiProductResponse;
			for(i=0; i<products.apiProduct.length; i++) {
        if (productNames.length == 0) {
          var api: apiProduct = {
            name: products.apiProduct[i].name,
            displayName: products.apiProduct[i].displayName,
            description: products.apiProduct[i].description,
            status: "subscribe", 
            specUrl: undefined
          };

          if (products.apiProduct[i].attributes.length > 0) {
            for (var a=0; a<products.apiProduct[i].attributes.length; a++) {
              var attr = products.apiProduct[i].attributes[a];
              if (attr.name == "specurl") {
                api.specUrl = attr.value;
              }
            }
          }

          result.push(api);
        }
        else if (productNames.includes(products.apiProduct[i].name)) {
          let api: apiProduct = result.find(x => x.name === products.apiProduct[i].name) as apiProduct;
          api.status = "subscribe";
        }
			}

      //console.log(result);

			res.send(result);
        }).catch(function (error: any) {
			console.log(error);
        res.status(500).send({message: error});
      }).then(function () {
    });
	}, function(err: any) {
    console.log(err);
		res.status(500).send({message: err});
	});
});

// Creates an app with keys
app.post('/developers/:devId/apps', (req: any, res: any) => {

	console.log("create app request received for developer " + req.headers.email);

	var devEmail = req.headers.email;
	var apiProduct = req.body.apiProduct;

  let org: string = process.env.APIGEE_ORG as string;
  if (config) {
    var api = config.portal.products.find(x => x.apiProductName === apiProduct);
    if (api && api.apigeeOrg) {
      org = api.apigeeOrg;
    }
  }

	apigeeService.getApigeeToken().then(function(token: string) {
    apigeeService.createDevApp(token, devEmail, apiProduct, org).then(function(result: any) {
			res.status(201).send(result);
    }).catch(function(error: any) {
      res.status(500).send(error);
    });
	}, function(err: any) {
		res.status(500).send({message: err});
	});
});

// Deletes an app
app.delete('/developers/:devId/apps/:appName', (req: any, res: any) => {

	console.log("delete app request received for developer " + req.headers.email + " and app " + req.params.appName);
	console.log("payload recieved: " + JSON.stringify(req.body));

	var devEmail = req.headers.email;
	var appName = req.params.appName;

  var org = process.env.APIGEE_ORG;
  if (config) {
    var api = config.portal.products.find(x => x.apiProductName === appName);
    if (api && api.apigeeOrg) {
      org = api.apigeeOrg;
    }
  }

	apigeeService.getApigeeToken().then(function(token: string) {
		var config = {
			url: "https://api.enterprise.apigee.com/v1/organizations/" + org + "/developers/" + devEmail + "/apps/" + appName,
			method: "delete",
			headers: {
				Authorization: "Bearer " + token,
				"Content-Type": "application/json"
			}
		};

		axios(config).then(function (response: any) {
			console.log("delete app status " + response.status);
			if (response.status == 200) {
				console.log("Successfully deleted app");
				// Now send info back to the client..
				res.status(200).send();
			}
			else
				res.status(500).send(response.data);
        }).catch(function (error: any) {
			console.log(error);
            res.status(500).send({message: error});
        }).then(function () {
      	});
	}, function(err: any) {
		res.status(500).send({message: err});
	});
});

// Gets all the developer's apps
app.get('/developers/:devId/apps', (req: any, res: any) => {

  console.log("enter developer apps get.. + " + JSON.stringify(orgs, null, '\t'));

	apigeeService.getApigeeToken().then(function(token: string) {

    var promises = [];
    for (i=0; i<orgs.length; i++) {
      var org = orgs[i];

      promises.push(apigeeService.getDevApps(token, req.headers.email, org));
    }

    Promise.all(promises).then((result: app[][]) => {
      var resultArray: app[] = [];
      for (i=0; i<result.length; i++) {
        resultArray = resultArray.concat(result[i]);
      }
			res.status(200).send(resultArray);
    }).catch(function (error) {
			console.log(error);
      res.status(500).send({message: error});
    });
	}, function(err: any) {
		res.status(500).send({message: err});
	});
});

// Gets all the rate plans
app.get('/rateplans', (req: any, res: any) => {

	apigeeService.getApigeeToken().then(function(token: string) {

    var promises = [];
    for (i=0; i<orgs.length; i++) {
      let org: string = orgs[i];
      promises.push(apigeeService.getRatePlans(token, org));
    }

    Promise.all(promises).then((result: rate[][]) => {
      var resultArray: rate[] = [];
      var finalResultArray: rate[] = [];

      for (i=0; i<result.length; i++) {
        resultArray = resultArray.concat(result[i]);
      }

      if(config.portal.rateplans && config.portal.rateplans.length > 0) {
        for(let i=resultArray.length-1; i>=0; i--) {
          var configuredRate = config.portal.rateplans.find(x => x.name == resultArray[i].name);

          if (!configuredRate) {
          //if (!config.portal.rateplans.includes(resultArray[i].name)) {
            resultArray.splice(i, 1);
          }
          else {
            resultArray[i].image = configuredRate.image;
            finalResultArray[config.portal.rateplans.indexOf(configuredRate)] = resultArray[i];
          }
        }
      }
      else
        finalResultArray = resultArray;

			res.status(200).send(finalResultArray);
    }).catch(function (error) {
			console.log(error);
      res.status(500).send({message: error});
    });
	}, function(err: any) {
		res.status(500).send({message: err});
	});
});

// Purchases a rate plan
app.post('/rateplans/:planid', (req: any, res: any) => {

	//console.log("create app request received for developer " + req.headers.email);

	var devEmail = req.headers.email;
	var rateId = req.params.planid;;

  let org: string = process.env.APIGEE_ORG as string;

	apigeeService.getApigeeToken().then(function(token: string) {
    apigeeService.createDevRateSub(token, devEmail, rateId, org).then(function(result: devRate) {
			res.status(201).send(result);
    }).catch(function(error: any) {
      res.status(500).send(error);
    });
	}, function(err: any) {
		res.status(500).send({message: err});
	});
});

// Delete purchased rate plan
app.delete('/rateplans/:planid/:plansubid', (req: any, res: any) => {

  var devEmail = req.headers.email;
  var rateId = req.params.planid;
	var rateSubId = req.params.plansubid;

  let org: string = process.env.APIGEE_ORG as string;

	apigeeService.getApigeeToken().then(function(token: string) {
    apigeeService.deleteDevRateSub(token, devEmail, rateId, rateSubId, org).then(function(result: devRate) {
			res.status(200).send(result);
    }).catch(function(error: any) {
      res.status(500).send(error);
    });
	}, function(err: any) {
		res.status(500).send({message: err});
	});
});

// Purchases credits
app.post('/rateplans/:currency/credits', (req: any, res: any) => {

	//console.log("create app request received for developer " + req.headers.email);

	var devEmail = req.headers.email;
  var currency = req.params.currency;
  var amount = req.body.amount;

  let org: string = process.env.APIGEE_ORG as string;

	apigeeService.getApigeeToken().then(function(token: string) {
    apigeeService.addDevCredits(token, devEmail, currency, amount, org).then(function(result: devCreditAccount) {
			res.status(201).send(result);
    }).catch(function(error: any) {
      res.status(500).send(error);
    });
	}, function(err: any) {
		res.status(500).send({message: err});
	});
});

app.listen(8080, () => console.log('apigee-portal service started!'));
