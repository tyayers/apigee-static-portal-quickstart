// Apigee structures

interface apigeeApiProduct {
  name: string;
  displayName: string;
  description: string;
  attributes: keyValue[];
}

interface apigeeApps {
  app: apigeeApp[];
}

interface apigeeApp {
  appId: string;
  name: string;
  status: string;
  credentials: apigeeAppCredentials[];
}

interface apigeeAppCredentials {
  consumerKey: string;
  consumerSecret: string;
  apiProducts: apigeeApiProductName[];
}

interface apigeeApiProductName {
  apiproduct: string;
}

interface apigeeAccessToken {
  access_token: string;
}

interface apigeeDeveloper {
  developerId?: string;
  email: string;
  firstName: string;
  lastName: string;
  userName: string;
  status?: string;
  billingType?: string;
  type?: string;
  organization?: apigeeDeveloperOrganization;
  attributes: keyValue[];
}

interface apigeeDeveloperOrganization {
  currency: string;
  country: string;
}

interface apigeeApiProductResponse {
  apiProduct: apigeeApiProduct[];
}

interface apigeeRatePlanResponse {
  ratePlan: apigeeRatePlan[];
}

interface apigeeDevRatePlanResponse {
  developerRatePlan: apigeeDevRatePlan[];
}

interface apigeeDevCreditBalanceResponse {
  developerBalance: apigeeBalance[];
}

interface apigeeDevRatePlan {
  id: string;
  nextCycleStartDate: string;
  nextRecurringFeeDate: string;
  renewalDate: string;
  startDate: string;
  endDate: string;
  quotaTarget: string;
  ratePlan: apigeeRatePlan;
}

interface apigeeRatePlan {
  id: string;
  displayName: string;
  description: string;
  name: string;
  published: boolean;
  setUpFee: number;
  recurringFee: number;
  recurringType: string;
  startDate: string;
  endDate: string;
  currency: apigeeCurrency;
  ratePlanDetails: apigeeRatePlanDetails[];
}

interface apigeeRatePlanDetails {
  meteringType: string;
  durationType: string;
  ratingParameter: string;
  revenueType: string;
  ratePlanRates: apigeeRatePlanRates[];
}

interface apigeeRatePlanRates {
  ratingParameter: string;
  revenueType: string;
  type: string;
  startUnit: number;
  endUnit: number;
  rate: number;
}

interface apigeeBalance {
  id: string;
  isRecurring: boolean;
  supportedCurrency?: apigeeCurrency;
  chargePerUsage: boolean;
  usage: number;
  amount: number;  
}

interface apigeeCurrency {
  id: string;
  name: string;
  displayName: string;
}

// Neutral DTO structures

interface developer {
  developerId?: string;
  email: string;
  firstName: string;
  lastName: string;
  userName: string;
  currency: string;
  country: string;
  status: string;
  billingType: string;
  type: string;
  attributes: keyValue[];
  devApps?: app[];
  devRatePlans?: devRate[];
  devCreditAccounts?: devCreditAccount[];
}

interface keyValue {
  name: string;
  value: string;
}

interface app {
  appId: string;
  apiProduct: string;
  status: string;
  key: string;
  secret: string;
}

interface devRate {
  id: string;
  nextCycleStartDate: string;
  nextRecurringFeeDate: string;
  renewalDate: string;
  startDate: string;
  endDate: string;
  quotaTarget: string;
  ratePlan: rate;
}

interface rate {
  id: string;
  name: string;
  image?: string;
  ratePlanDetails: rateDetail[];
}

interface rateDetail {
  meteringType: string;
  durationType: string;
  ratingParameter: string;
  revenueType: string;
  ratePlanRates: rateDetailRates[];
}

interface rateDetailRates {
  type: string;
  startUnit: number;
  endUnit: number;
  rate: number;  
}

interface devCreditAccount {
  id: string;
  currency: string;
  isRecurring: boolean;
  chargePerUsage: boolean;
  usage: number;
  amount: number;
}

interface apiProduct {
  name: string;
  displayName: string;
  description: string;
  image?: string;
  specUrl?: string;
  status: string;
}

interface response {
  status: number;
  data: object;
}

interface error {
  response: response;
}

// Class interfaces
interface ApigeeInterface {
  createDevApp(token: string, devEmail: string, apiProduct: string, org: string): Promise<app>;
  createDevRateSub(token: string, devEmail: string, ratePlanId: string, org: string): Promise<devRate>;
  deleteDevRateSub(token: string, devEmail: string, ratePlanId: string, rateSubId: string, org: string): Promise<devRate>;
  getRatePlans(token: string, org: string): Promise<rate[]>;
  addDevCredits(token: string, devEmail: string, currency: string, amount: number, org: string): Promise<devCreditAccount>;
  createOrUpdateApigeeDev(token: string, devData: apigeeDeveloper, org: string): Promise<developer>;
  getDev(token: string, devEmail: string, org:string): Promise<developer>;
  getDevApps(token: string, devEmail: string, org: string): Promise<app[]>;
  getDevRatePlans(token: string, devEmail: string, org: string): Promise<devRate[]>;
  getDevCreditAccounts(token: string, devEmail: string, org: string): Promise<devCreditAccount[]>;
  getApigeeToken(): Promise<string>;
}

// Function interfaces
interface GetRatePlansFunc {
  (token: string, org: string): Promise<rate[]>;
}