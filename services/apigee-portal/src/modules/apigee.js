"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Apigee = void 0;
var axios = require('axios');
require('../dtos/types');
var Apigee = /** @class */ (function () {
    function Apigee() {
    }
    Apigee.prototype.getRatePlans = function (token, org) {
        return new Promise(function (resolve, reject) {
            var result = [];
            var config = {
                url: "https://api.enterprise.apigee.com/v1/mint/organizations/" + org + "/rate-plans?all=true",
                method: "get",
                headers: {
                    Authorization: "Bearer " + token,
                    "Content-Type": "application/json"
                }
            };
            axios(config).then(function (response) {
                var ratePlans = response.data;
                result = ratePlans.ratePlan;
                resolve(result);
            }).catch(function (error) {
                reject(error);
            });
        });
    };
    Apigee.prototype.addDevCredits = function (token, devEmail, currency, amount, org) {
        return new Promise(function (resolve, reject) {
            var result = [];
            var config = {
                url: "https://api.enterprise.apigee.com/v1/mint/organizations/" + org + "/developers/" + devEmail + "/developer-balances",
                method: "post",
                headers: {
                    Authorization: "Bearer " + token,
                    "Content-Type": "application/json"
                },
                data: {
                    amount: amount,
                    supportedCurrency: {
                        id: currency
                    }
                }
            };
            axios(config).then(function (response) {
                var accountInfo = response.data;
                resolve(accountInfo);
            }).catch(function (error) {
                reject(error);
            });
        });
    };
    Apigee.prototype.createDevApp = function (token, devEmail, apiProduct, org) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var config = {
                url: "https://api.enterprise.apigee.com/v1/organizations/" + org + "/developers/" + devEmail + "/apps",
                method: "post",
                headers: {
                    Authorization: "Bearer " + token,
                    "Content-Type": "application/json"
                },
                data: {
                    name: apiProduct,
                    apiProducts: [apiProduct],
                    attributes: [],
                    scopes: []
                }
            };
            axios(config).then(function (response) {
                //console.log("create-app status " + response.status);
                //console.log("create-app success: " + JSON.stringify(response.data, null, '\t'));
                if (response.status == 201) {
                    //console.log("Successfully created app");
                    // Now send info back to the client..
                    var appData = response.data;
                    var result = {
                        appId: appData.name,
                        apiProduct: apiProduct,
                        status: appData.status,
                        key: appData.credentials[0].consumerKey,
                        secret: appData.credentials[0].consumerSecret
                    };
                    resolve(result);
                }
                else
                    reject({ message: response.data });
            }).catch(function (error) {
                console.log(error);
                if (error.response.status == 404) {
                    // dev doesn't exist in org, create...
                    console.log("dev doesn't exist, create...");
                    var devData = {
                        email: devEmail,
                        firstName: "dev",
                        lastName: "dev",
                        userName: devEmail,
                        attributes: []
                    };
                    _this.createOrUpdateApigeeDev(token, devData, org).then(function (result) {
                        _this.createDevApp(token, devEmail, apiProduct, org).then(function (result) {
                            //console.log("dev create successful..");
                            resolve(result);
                        }).catch(function (error) {
                            console.log(error);
                            reject(error);
                        });
                    }).catch(function (error) {
                        console.log(error);
                        reject(error);
                    });
                }
            });
        });
    };
    Apigee.prototype.createDevRateSub = function (token, devEmail, ratePlanId, org) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var startDate = new Date();
            var endDate = new Date();
            endDate.setMonth(endDate.getMonth() + 6);
            var config = {
                url: "https://api.enterprise.apigee.com/v1/mint/organizations/" + org + "/developers/" + devEmail + "/developer-rateplans",
                method: "post",
                headers: {
                    Authorization: "Bearer " + token,
                    "Content-Type": "application/json"
                },
                data: {
                    developer: {
                        id: devEmail
                    },
                    endDate: endDate.toISOString().replace("T", " ").replace("Z", ""),
                    startDate: startDate.toISOString().replace("T", " ").replace("Z", ""),
                    ratePlan: {
                        id: ratePlanId
                    },
                    suppressWarning: false,
                    waveTerminationCharge: false,
                    quotaTarget: 0
                }
            };
            axios(config).then(function (response) {
                //console.log("create-app status " + response.status);
                //console.log("create-app success: " + JSON.stringify(response.data, null, '\t'));
                if (response.status == 201) {
                    //console.log("Successfully created app");
                    // Now send info back to the client..
                    var rateSubData = response.data;
                    resolve(rateSubData);
                }
                else
                    reject({ message: response.data });
            }).catch(function (error) {
                console.log(error);
                if (error.response.status == 404) {
                    // dev doesn't exist in org, create...
                    console.log("dev doesn't exist, create...");
                    var devData = {
                        email: devEmail,
                        firstName: "dev",
                        lastName: "dev",
                        userName: devEmail,
                        attributes: []
                    };
                    _this.createOrUpdateApigeeDev(token, devData, org).then(function (result) {
                        _this.createDevRateSub(token, devEmail, ratePlanId, org).then(function (result) {
                            console.log("dev create successful..");
                            resolve(result);
                        }).catch(function (error) {
                            console.log(error);
                            reject(error);
                        });
                    }).catch(function (error) {
                        console.log(error);
                        reject(error);
                    });
                }
                else
                    reject(error);
            });
        });
    };
    Apigee.prototype.deleteDevRateSub = function (token, devEmail, ratePlanId, rateSubId, org) {
        return new Promise(function (resolve, reject) {
            var startDate = new Date();
            var endDate = new Date();
            endDate.setMinutes(endDate.getMinutes() + 2);
            //endDate.setMonth(endDate.getMonth() + 6);
            var config = {
                url: "https://api.enterprise.apigee.com/v1/mint/organizations/" + org + "/developers/" + devEmail + "/developer-rateplans/" + rateSubId,
                method: "put",
                headers: {
                    Authorization: "Bearer " + token,
                    "Content-Type": "application/json"
                },
                data: {
                    id: rateSubId,
                    developer: {
                        id: devEmail
                    },
                    endDate: endDate.toISOString().replace("T", " ").replace("Z", ""),
                    ratePlan: {
                        id: ratePlanId
                    },
                    suppressWarning: false,
                    waveTerminationCharge: false,
                    quotaTarget: 0
                }
            };
            axios(config).then(function (response) {
                //console.log("create-app status " + response.status);
                //console.log("create-app success: " + JSON.stringify(response.data, null, '\t'));
                if (response.status == 200) {
                    //console.log("Successfully created app");
                    // Now send info back to the client..
                    var rateSubData = response.data;
                    resolve(rateSubData);
                }
                else
                    reject({ message: response.data });
            }).catch(function (error) {
                console.error(error);
            });
        });
    };
    Apigee.prototype.getDev = function (token, devEmail, org) {
        return new Promise(function (resolve, reject) {
            var result;
            var config = {
                url: "https://api.enterprise.apigee.com/v1/organizations/" + org + "/developers/" + devEmail + "?expand=true",
                method: "get",
                headers: {
                    Authorization: "Bearer " + token,
                    "Content-Type": "application/json"
                }
            };
            axios(config).then(function (response) {
                var devResponse = response.data;
                result = devResponse;
                if (devResponse.organization) {
                    result.currency = devResponse.organization.currency;
                    result.country = devResponse.organization.country;
                }
                else {
                    // get mint org info if it exists
                    config = {
                        url: "https://api.enterprise.apigee.com/v1/mint/organizations/" + org,
                        method: "get",
                        headers: {
                            Authorization: "Bearer " + token,
                            "Content-Type": "application/json"
                        }
                    };
                    axios(config).then(function (response) {
                        var orgResponse = response.data;
                        result.currency = orgResponse.currency;
                        result.country = orgResponse.country;
                        resolve(result);
                    }).catch(function (error) {
                        resolve(result);
                    });
                }
            }).catch(function (error) {
                reject(error);
            });
        });
    };
    Apigee.prototype.createOrUpdateApigeeDev = function (token, devData, org) {
        return new Promise(function (resolve, reject) {
            var config = {
                url: "https://api.enterprise.apigee.com/v1/organizations/" + org + "/developers",
                method: "post",
                headers: {
                    Authorization: "Bearer " + token,
                    "Content-Type": "application/json"
                },
                data: devData
            };
            axios(config).then(function (response) {
                //console.log("create-dev status " + response.status);
                //console.log("create-dev result: " + JSON.stringify(response.data, null, '\t'));
                if (response.status == 201) {
                    var dev = response.data;
                    resolve(dev);
                }
            }).catch(function (error) {
                console.log("Exception, got response ");
                console.log(error);
                if (error.response.status == 409) {
                    // dev already exists, then is ok
                    console.log("no exception, got response 409");
                    config.method = "put";
                    config.url += "/" + devData.email;
                    axios(config).then(function (response) {
                        console.log("update-dev status " + response.status);
                        var dev = response.data;
                        resolve(dev);
                    }).catch(function (error) {
                        reject(error);
                    });
                }
                else {
                    reject(error);
                }
            }).then(function () {
            });
        });
    };
    Apigee.prototype.getDevApps = function (token, devEmail, org) {
        return new Promise(function (resolve, reject) {
            var result = [];
            var config = {
                url: "https://api.enterprise.apigee.com/v1/organizations/" + org + "/developers/" + devEmail + "/apps?expand=true",
                method: "get",
                headers: {
                    Authorization: "Bearer " + token,
                    "Content-Type": "application/json"
                }
            };
            axios(config).then(function (response) {
                //console.log("get-products success: " + JSON.stringify(response.data));
                var appResponse = response.data;
                for (var i = 0; i < appResponse.app.length; i++) {
                    var app = {
                        appId: appResponse.app[i].appId,
                        apiProduct: appResponse.app[i].credentials[0].apiProducts[0].apiproduct,
                        status: appResponse.app[i].status,
                        key: appResponse.app[i].credentials[0].consumerKey,
                        secret: appResponse.app[i].credentials[0].consumerSecret
                    };
                    result.push(app);
                }
                resolve(result);
            }).catch(function (error) {
                reject(error);
            });
        });
    };
    Apigee.prototype.getDevRatePlans = function (token, devEmail, org) {
        return new Promise(function (resolve, reject) {
            var result = [];
            var config = {
                url: "https://api.enterprise.apigee.com/v1/mint/organizations/" + org + "/developers/" + devEmail + "/developer-accepted-rateplans",
                method: "get",
                headers: {
                    Authorization: "Bearer " + token,
                    "Content-Type": "application/json"
                }
            };
            axios(config).then(function (response) {
                var devRateResponse = response.data;
                for (var i = 0; i < devRateResponse.developerRatePlan.length; i++) {
                    var newDevRate = devRateResponse.developerRatePlan[i];
                    var now = new Date();
                    var startDate = new Date(newDevRate.startDate);
                    var endDate = new Date(newDevRate.endDate);
                    if (now > startDate && now < endDate)
                        result.push(newDevRate);
                }
                resolve(result);
            }).catch(function (error) {
                reject(error);
            });
        });
    };
    Apigee.prototype.getDevCreditAccounts = function (token, devEmail, org) {
        return new Promise(function (resolve, reject) {
            var result = [];
            var config = {
                url: "https://api.enterprise.apigee.com/v1/mint/organizations/" + org + "/developers/" + devEmail + "/developer-balances",
                method: "get",
                headers: {
                    Authorization: "Bearer " + token,
                    "Content-Type": "application/json"
                }
            };
            axios(config).then(function (response) {
                var _a;
                var devCreditResponse = response.data;
                for (var i = 0; i < devCreditResponse.developerBalance.length; i++) {
                    var newDevAccount = devCreditResponse.developerBalance[i];
                    newDevAccount.currency = (_a = devCreditResponse.developerBalance[i].supportedCurrency) === null || _a === void 0 ? void 0 : _a.name;
                    result.push(newDevAccount);
                }
                resolve(result);
            }).catch(function (error) {
                reject(error);
            });
        });
    };
    Apigee.prototype.getApigeeToken = function () {
        return new Promise(function (resolve, reject) {
            axios.post("https://login.apigee.com/oauth/token", "username=" + process.env.APIGEE_USER + "&password=" + process.env.APIGEE_PW + "&grant_type=password", {
                headers: {
                    "Content-Type": "application/x-www-form-urlencoded;charset=utf-8",
                    "Accept": "application/json;charset=utf-8",
                    "Authorization": "Basic ZWRnZWNsaTplZGdlY2xpc2VjcmV0"
                }
            }).then(function (response) {
                var token = response.data;
                resolve(token.access_token);
            })
                .catch(function (error) {
                console.log(error);
                reject("Error during token request: " + error);
            });
        });
    };
    return Apigee;
}());
exports.Apigee = Apigee;
