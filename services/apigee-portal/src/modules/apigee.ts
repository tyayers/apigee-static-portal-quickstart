export {};

let axios = require('axios');
require('../dtos/types');

export class Apigee implements ApigeeInterface {
  constructor() {}

  getRatePlans(token: string, org: string): Promise<rate[]> {
    return new Promise((resolve, reject) => {
      var result: rate[] = [];
  
      var config = {
        url: "https://api.enterprise.apigee.com/v1/mint/organizations/" + org + "/rate-plans?all=true",
        method: "get",
        headers: {
          Authorization: "Bearer " + token,
          "Content-Type": "application/json"
        }
      };
  
      axios(config).then((response: response) => {
  
        let ratePlans: apigeeRatePlanResponse = response.data as apigeeRatePlanResponse;
        result = ratePlans.ratePlan as rate[];
  
        resolve(result);
      }).catch((error: error) => {
        reject(error);
      });
    });  
  }

  addDevCredits(token: string, devEmail: string, currency: string, amount: number, org: string): Promise<devCreditAccount> {
    return new Promise((resolve, reject) => {
      var result: rate[] = [];
  
      var config = {
        url: "https://api.enterprise.apigee.com/v1/mint/organizations/" + org + "/developers/" + devEmail + "/developer-balances",
        method: "post",
        headers: {
          Authorization: "Bearer " + token,
          "Content-Type": "application/json"
        },
        data: {
          amount: amount,
          supportedCurrency: {
            id: currency
          }          
        }
      };
  
      axios(config).then((response: response) => {
  
        let accountInfo: devCreditAccount = response.data as devCreditAccount;
        resolve(accountInfo);
      }).catch((error: error) => {
        reject(error);
      });
    });  
  }

  createDevApp(token: string, devEmail: string, apiProduct: string, org: string): Promise<app> {
    return new Promise((resolve, reject) => {
      let config = {
        url: "https://api.enterprise.apigee.com/v1/organizations/" + org + "/developers/" + devEmail + "/apps",
        method: "post",
        headers: {
          Authorization: "Bearer " + token,
          "Content-Type": "application/json"
        },
        data: {
          name: apiProduct,
          apiProducts: [ apiProduct ],
          attributes: [],
          scopes: []
        }
      };
      axios(config).then((response: response) => {
        //console.log("create-app status " + response.status);
        //console.log("create-app success: " + JSON.stringify(response.data, null, '\t'));
        if (response.status == 201) {
          //console.log("Successfully created app");
          // Now send info back to the client..
          let appData: apigeeApp = response.data as apigeeApp;
          let result: app = {
            appId: appData.name, 
            apiProduct: apiProduct,
            status: appData.status,
            key: appData.credentials[0].consumerKey, 
            secret: appData.credentials[0].consumerSecret
          };
  
          resolve(result);
        }
        else
          reject({message: response.data});
      }).catch( (error: error) => {
        console.log(error);
        if (error.response.status == 404) {
          // dev doesn't exist in org, create...
          console.log("dev doesn't exist, create...");
          var devData: apigeeDeveloper = {
            email: devEmail,
            firstName: "dev",
            lastName: "dev",
            userName: devEmail,
            attributes: []
          };
          
          this.createOrUpdateApigeeDev(token, devData, org).then((result: apigeeDeveloper) => {
            this.createDevApp(token, devEmail, apiProduct, org).then((result: app) => {
              //console.log("dev create successful..");
              resolve(result);
            }).catch((error: error) => {
              console.log(error);
              reject(error);
            });
          }).catch((error: error) => {
            console.log(error);
            reject(error);
          });
        }    
      });
    });
  }

  createDevRateSub(token: string, devEmail: string, ratePlanId: string, org: string): Promise<devRate> {
    return new Promise((resolve, reject) => {
      var startDate = new Date();
      var endDate = new Date();
      endDate.setMonth(endDate.getMonth() + 6);
      let config = {
        url: "https://api.enterprise.apigee.com/v1/mint/organizations/" + org + "/developers/" + devEmail + "/developer-rateplans",
        method: "post",
        headers: {
          Authorization: "Bearer " + token,
          "Content-Type": "application/json"
        },
        data: {
          developer: {
            id: devEmail
          },
          endDate: endDate.toISOString().replace("T", " ").replace("Z", ""),
          startDate: startDate.toISOString().replace("T", " ").replace("Z", ""),
          ratePlan: {
            id: ratePlanId
          },
          suppressWarning: false,
          waveTerminationCharge: false,
          quotaTarget: 0
        }
      };
      axios(config).then((response: response) => {
        //console.log("create-app status " + response.status);
        //console.log("create-app success: " + JSON.stringify(response.data, null, '\t'));
        if (response.status == 201) {
          //console.log("Successfully created app");
          // Now send info back to the client..
          let rateSubData: apigeeDevRatePlan = response.data as apigeeDevRatePlan;
          resolve(rateSubData);
        }
        else
          reject({message: response.data});
      }).catch( (error: error) => {
        console.log(error);
        if (error.response.status == 404) {
          // dev doesn't exist in org, create...
          console.log("dev doesn't exist, create...");
          var devData = {
            email: devEmail,
            firstName: "dev",
            lastName: "dev",
            userName: devEmail,
            attributes: []
          };
          
          this.createOrUpdateApigeeDev(token, devData, org).then((result: apigeeDeveloper) => {
            this.createDevRateSub(token, devEmail, ratePlanId, org).then((result: devRate) => {
              console.log("dev create successful..");
              resolve(result);
            }).catch((error: error) => {
              console.log(error);
              reject(error);
            });
          }).catch((error: error) => {
            console.log(error);
            reject(error);
          });
        }
        else
          reject(error);    
      });
    });
  }

  deleteDevRateSub(token: string, devEmail: string, ratePlanId: string, rateSubId: string, org: string): Promise<devRate> {
    return new Promise((resolve, reject) => {
      var startDate = new Date();
      var endDate = new Date();
      endDate.setMinutes(endDate.getMinutes() + 2);
      //endDate.setMonth(endDate.getMonth() + 6);
      let config = {
        url: "https://api.enterprise.apigee.com/v1/mint/organizations/" + org + "/developers/" + devEmail + "/developer-rateplans/" + rateSubId,
        method: "put",
        headers: {
          Authorization: "Bearer " + token,
          "Content-Type": "application/json"
        },
        data: {
          id: rateSubId,
          developer: {
            id: devEmail
          },
          endDate: endDate.toISOString().replace("T", " ").replace("Z", ""),
          ratePlan: {
            id: ratePlanId
          },
          suppressWarning: false,
          waveTerminationCharge: false,
          quotaTarget: 0
        }
      };
      axios(config).then((response: response) => {
        //console.log("create-app status " + response.status);
        //console.log("create-app success: " + JSON.stringify(response.data, null, '\t'));
        if (response.status == 200) {
          //console.log("Successfully created app");
          // Now send info back to the client..
          let rateSubData: apigeeDevRatePlan = response.data as apigeeDevRatePlan;
          resolve(rateSubData);
        }
        else
          reject({message: response.data});
      }).catch( (error: error) => {
        console.error(error);
      });
    });
  }

  getDev(token: string, devEmail: string, org:string): Promise<developer> {
    return new Promise((resolve, reject) => {
      let result: developer;
  
      var config = {
        url: "https://api.enterprise.apigee.com/v1/organizations/" + org + "/developers/" + devEmail + "?expand=true",
        method: "get",
        headers: {
          Authorization: "Bearer " + token,
          "Content-Type": "application/json"
        }
      };

      axios(config).then((response: response) => {
        let devResponse: apigeeDeveloper = response.data as apigeeDeveloper;
        result = devResponse as developer;
        if (devResponse.organization) {
          result.currency = devResponse.organization.currency;
          result.country = devResponse.organization.country;
        }
        else {
          // get mint org info if it exists
          config = {
            url: "https://api.enterprise.apigee.com/v1/mint/organizations/" + org,
            method: "get",
            headers: {
              Authorization: "Bearer " + token,
              "Content-Type": "application/json"
            }
          };
          
          axios(config).then((response: response) => {
            let orgResponse: apigeeDeveloperOrganization = response.data as apigeeDeveloperOrganization;
            result.currency = orgResponse.currency;
            result.country = orgResponse.country;
            resolve(result);
          }).catch((error: error) => {
            resolve(result);
          });
        }
      }).catch((error: error) => {
        reject(error);
      });
    });    
  }

  createOrUpdateApigeeDev(token: string, devData: apigeeDeveloper, org: string): Promise<developer> {
    return new Promise((resolve, reject) => {
      var config = {
        url: "https://api.enterprise.apigee.com/v1/organizations/" + org + "/developers",
        method: "post",
        headers: {
          Authorization: "Bearer " + token,
          "Content-Type": "application/json"
        },
        data: devData
      };
  
      axios(config).then( (response: response) => {
        //console.log("create-dev status " + response.status);
        //console.log("create-dev result: " + JSON.stringify(response.data, null, '\t'));
  
        if (response.status == 201) {
          let dev: apigeeDeveloper = response.data as apigeeDeveloper;
          resolve(dev as developer);
        }
        }).catch( (error: error) => {
          console.log("Exception, got response ");
          console.log(error);
          if (error.response.status == 409) {
            // dev already exists, then is ok
            console.log("no exception, got response 409");
            config.method = "put";
            config.url += "/" + devData.email;
  
            axios(config).then( (response: response) => {
              console.log("update-dev status " + response.status);
              let dev: apigeeDeveloper = response.data as apigeeDeveloper;
              resolve(dev as developer);
            }).catch( (error: error) => {
              reject(error);
            });                
          }
          else {
            reject(error);
          }            
        }).then( () => {
      });
    });
  }  

  getDevApps(token: string, devEmail: string, org: string): Promise<app[]> {
    return new Promise((resolve, reject) => {
      let result: app[] = [];
  
      var config = {
        url: "https://api.enterprise.apigee.com/v1/organizations/" + org + "/developers/" + devEmail + "/apps?expand=true",
        method: "get",
        headers: {
          Authorization: "Bearer " + token,
          "Content-Type": "application/json"
        }
      };
  
      axios(config).then( (response: response) => {
  
        //console.log("get-products success: " + JSON.stringify(response.data));
        let appResponse: apigeeApps = response.data as apigeeApps;
        for(let i=0; i<appResponse.app.length; i++) {
          var app: app = {
            appId: appResponse.app[i].appId,
            apiProduct: appResponse.app[i].credentials[0].apiProducts[0].apiproduct,
            status: appResponse.app[i].status,
            key: appResponse.app[i].credentials[0].consumerKey,
            secret: appResponse.app[i].credentials[0].consumerSecret
          };
  
          result.push(app);
        }
  
        resolve(result);
      }).catch((error: error) => {
        reject(error);
      });
    });
  }

  getDevRatePlans(token: string, devEmail: string, org: string): Promise<devRate[]> {
    return new Promise((resolve, reject) => {
      let result: devRate[] = [];
  
      var config = {
        url: "https://api.enterprise.apigee.com/v1/mint/organizations/" + org + "/developers/" + devEmail + "/developer-accepted-rateplans",
        method: "get",
        headers: {
          Authorization: "Bearer " + token,
          "Content-Type": "application/json"
        }
      };
  
      axios(config).then( (response: response) => {
  
        let devRateResponse: apigeeDevRatePlanResponse = response.data as apigeeDevRatePlanResponse;
        for(let i=0; i<devRateResponse.developerRatePlan.length; i++) {
          let newDevRate: devRate = devRateResponse.developerRatePlan[i];
          let now: Date = new Date();
          let startDate: Date = new Date(newDevRate.startDate);
          let endDate: Date = new Date(newDevRate.endDate);
          if (now > startDate && now < endDate)
            result.push(newDevRate);
        }
  
        resolve(result);
      }).catch((error: error) => {
        reject(error);
      });
    });
  }

  getDevCreditAccounts(token: string, devEmail: string, org: string): Promise<devCreditAccount[]> {
    return new Promise((resolve, reject) => {
      let result: devCreditAccount[] = [];
  
      var config = {
        url: "https://api.enterprise.apigee.com/v1/mint/organizations/" + org + "/developers/" + devEmail + "/developer-balances",
        method: "get",
        headers: {
          Authorization: "Bearer " + token,
          "Content-Type": "application/json"
        }
      };
  
      axios(config).then( (response: response) => {
  
        let devCreditResponse: apigeeDevCreditBalanceResponse = response.data as apigeeDevCreditBalanceResponse;
        for(let i=0; i<devCreditResponse.developerBalance.length; i++) {
          let newDevAccount: devCreditAccount = devCreditResponse.developerBalance[i] as devCreditAccount;
          newDevAccount.currency = devCreditResponse.developerBalance[i].supportedCurrency?.name as string;
          result.push(newDevAccount);
        }
  
        resolve(result);
      }).catch((error: error) => {
        reject(error);
      });
    });
  }

  getApigeeToken(): Promise<string> {
    return new Promise((resolve, reject) => {
      axios.post(
        "https://login.apigee.com/oauth/token",
        "username=" + process.env.APIGEE_USER + "&password=" + process.env.APIGEE_PW + "&grant_type=password",
        {
          headers: {
            "Content-Type": "application/x-www-form-urlencoded;charset=utf-8",
                  "Accept": "application/json;charset=utf-8",
                  "Authorization": "Basic ZWRnZWNsaTplZGdlY2xpc2VjcmV0"
          }
        }
      ).then( (response: response) => {
        let token: apigeeAccessToken = response.data as apigeeAccessToken;
        resolve(token.access_token);
      })
      .catch( (error: error) => {
        console.log(error);
        reject("Error during token request: " + error);
      });
    });
  }  
}
