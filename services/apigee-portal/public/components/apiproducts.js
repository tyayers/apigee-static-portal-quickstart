Vue.component('apiproducts', {
  template: `
    <div>
        <page-header />
        <main>
           		
            <section id="api-products">
                <header>
                    <h3>API Products</h3>
                    <p>Check out our amazing collection of curated & rarefied APIs</p>
                </header>

                <div v-if="this.$root.apiProducts.length == 0" class="card-loader card-loader--tabs" style="width: 280px; margin: 10px"></div>
                <div v-if="this.$root.apiProducts.length == 0" class="card-loader card-loader--tabs" style="width: 280px; margin: 10px"></div>
                <div v-if="this.$root.apiProducts.length == 0" class="card-loader card-loader--tabs" style="width: 280px; margin: 10px"></div>

                <aside v-for="(product, index) in this.$root.apiProducts" :key="product.name" style="position: relative; height: 400px; cursor: pointer;">
                    <div v-on:click="showDoc(product.name, index);">
                      <div style="width: 100%; text-align: center">
                        <img v-bind:src="product.image" height="150">
                        <h3 style="margin-top: 5px">{{product.displayName}}</h3>
                      </div>
                        <p>
                            {{product.description}}
                        </p>
                    </div>
                    <p v-if="product.subscribed" style="position: absolute; bottom: 5px">
                        <button type="button"  v-on:click="deleteApp(product.name)" class="btn btn-success btn-sm">Subscribed</button>
                        <a href="#" style="top: 2px; position: relative;" data-toggle="modal" v-bind:data-target="'#productmodal' + index">Show Keys ↗</a>

                        <div class="modal fade" v-bind:id="'productmodal' + index" tabindex="-1" role="dialog" v-bind:aria-labelledby="index + 'productlabelmodel'" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" v-bind:id="index + 'productmodellabel'">{{product.displayName}}</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <section>
                                            <table>
                                                <thead><tr><th/><th/></tr></thead>
                                                <tr><td>Key</td><td>{{product.key}}</td></tr>
                                                <tr><td>Secret</td><td style="text-align: left">{{product.secret}}</td></tr>
                                            </table>
                                        </section>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                            </div>
                        </div>                            
                    </p>
                    <p v-if="!product.subscribed && product.status == 'subscribe'" style="position: absolute; bottom: 5px">
                        <button type="button" v-on:click="createApp(product.name, $event)" class="btn btn-warning btn-sm click-subscribe" data-toggle="tooltip" data-placement="bottom" data-html="true" data-animation="true" title="Click to Subscribe">Not Subscribed</button>
                    </p>
                    <p v-if="product.status == 'documentation'" style="position: absolute; bottom: 5px">
                      <!--a v-on:click="showDoc(product.name, index);"><i>Public API 🕮</i></a-->
                      <button type="button" v-on:click="showDoc(product.name, index);" class="btn btn-info btn-sm">Public API 🕮</button>
                    </p>
                </aside>                   
            </section>              
        </main>

        <page-footer />
    </div>
    `,
    data: function () {
        return {
            
        }
    },
    created () {
        if (!this.$root.user)
            router.push("/");
        else
            this.fetchData();
    },
    beforeDestroy () {
        $('.click-subscribe').tooltip('dispose');
    },
    methods: {
        fetchData() {
            var user = this.$root.user;
            this.$root.user.getIdToken(false).then(function(token) {
                // First get developer's current apps
                var config = {
                    url: "/developers/" + user.email + "/apps",
                    method: "get",
                    headers: {
                        Authorization: "Bearer " + token,
                        "Content-Type": "application/json"
                    }
                };

                axios(config).then(function (response) {
                    //console.log(JSON.stringify(response.data, null, '\t'));
                    app.apps = response.data;
                }).catch(function (error) {
                    console.log("get apps error " + JSON.stringify(error));
                }).then(function () {

                    // Now get the api-products
                    var config = {
                        url: "/api-products",
                        method: "get",
                        headers: {
                            Authorization: "Bearer " + token,
                            "Content-Type": "application/json"
                        }
                    };

                    axios(config).then(function (response) {

                        app.apiProducts = [];

                        for(var i=0; i<response.data.length; i++) {
                            var apiProduct = response.data[i];
                            var appForProduct = getAppForProduct(apiProduct.name);

                            if (appForProduct != undefined) {
                                apiProduct.subscribed = true;
                                apiProduct.key = appForProduct.key;
                                apiProduct.secret = appForProduct.secret;
                            }
                            else {
                                apiProduct.subscribed = false;
                            }

                            app.apiProducts.push(apiProduct);
                        }
                    }).catch(function (error) {
                        console.error(error);
                    }).then(function () {
                        // setTimeout(function() {
                        //     $('.click-subscribe').tooltip('show');
                        //     setTimeout(function() {
                        //         $('.click-subscribe').tooltip('hide');
                        //     }, 4000);
                        // }, 1000);
                    });
                });
            });            
        },
        deleteApp(name) {
            var origButton = event.currentTarget;
            origButton.innerHTML = "Working...";            
            var apiProducts = this.$root.apiProducts;
            var apps = this.$root.apps;
            var user = this.$root.user;
            this.$root.user.getIdToken(false).then(function(token) {
                // First get developer's current apps
                var config = {
                    url: "/developers/" + user.email + "/apps/" + name,
                    method: "delete",
                    headers: {
                        Authorization: "Bearer " + token,
                        "Content-Type": "application/json"
                    }
                };

                axios(config).then(function (response) {
                    //console.log(JSON.stringify(response.data, null, '/t'));
                    if (response.status == 200) {
                        for(var i=0; i<apps.length; i++) {
                            if (apps[i].name == name) {
                                apps.splice(i, 1);
                                break;
                            }
                        }

                        for (var i=0; i<apiProducts.length; i++) {
                            var product = apiProducts[i];
                            if (product.name == name) {
                                product.subscribed = false;
                                product.key = undefined;
                                product.secret = undefined;
                                break;
                            }
                        }

                        origButton.innerHTML = "Not Subscribed";
                        origButton.title = "";
                    }
                }).catch(function (error) {
                    console.log("delete app error " + JSON.stringify(error));
                }).then(function () { });        
            });    
        },
        createApp(name, event) {
            var origButton = event.currentTarget;
            origButton.innerHTML = "Working...";
            var apiProducts = this.$root.apiProducts;
            var apps = this.$root.apps;
            var user = this.$root.user;
            this.$root.user.getIdToken(false).then(function(token) {
              var config = {
                url: "/developers/" + user.email + "/apps",
                method: "post",
                headers: {
                  Authorization: "Bearer " + token,
                  "Content-Type": "application/json"
                },
                data: {
                  apiProduct: name
                }
              };

              axios(config).then(function (response) {
                  apps.push(response.data);
                  for(var i=0; i<apiProducts.length; i++) {
                    var apiProduct = apiProducts[i];
                    if (apiProduct.name == response.data.apiProduct) {
                      apiProduct.subscribed = true;
                      apiProduct.key = response.data.key;
                      apiProduct.secret = response.data.secret;
                    }
                  }
                  //$('.click-subscribe').tooltip('dispose');
                  origButton.innerHTML = "Subscribed";
              }).catch(function (error) {
                  console.error(error);
              }).then(function () {

              });
            });
        },
        showDoc(name, index) {
          router.push({ path: `/api-products/${name}` });
        }
    }
});