Vue.component('page-navbar', {
  template: `
    <nav>
        <a href="/"><img alt="Logo" src="https://upload.wikimedia.org/wikipedia/commons/a/aa/Apigee_logo.svg" height="70"></a>
        <ul>
            <li v-if="!this.$root.user || !this.$root.user.emailVerified">
                <router-link to="/sign-in">Sign In</router-link>
            </li>
            <li v-if="this.$root.user && this.$root.user.emailVerified">
                <router-link to="/api-products">APIs</router-link>
            </li>               
            <li v-if="this.$root.user && this.$root.user.emailVerified">
                <router-link to="/rateplans">Pricing & Plans</router-link>
            </li>       
            <li v-if="this.$root.user && this.$root.user.emailVerified">
            <router-link to="/account">Account</router-link>
            </li>                                   
            <li v-if="this.$root.user && this.$root.user.emailVerified">
                <a href="#" v-on:click="signOff">Sign Out</a>
            </li>        
        </ul>
    </nav>
    `,
    methods: {
        signOff() {
            firebase.auth().signOut().catch(function(error) {
                // Handle Errors here.
                var errorCode = error.code;
                var errorMessage = error.message;
                alert("There was an error with the signout. " + error.message + " " + error.code);
            });
        }
    }
});