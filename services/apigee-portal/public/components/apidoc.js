Vue.component('apidoc', {
  template: `
    <div>
        <header>
            <page-navbar />
            <div style="width: 100%; text-align: left;">
              <img v-on:click="back()" style="cursor: pointer; opacity: .5;" src="img/button-arrow-left.png" width="70" />
            </div>
        </header>
        
        <main>
        	 <section style="margin-bottom: 25px">
            <img v-bind:src="this.$root.apiProducts[this.apiProductIndex].image" height="150">
           </section> 
            <section>
                
        
                <h1>{{this.$root.apiProducts[this.apiProductIndex].displayName}}</h1>
            </section>
            <section>
                <div id="swagger-ui" style="width: 100%;">

                </div>
            </section             
        </main>

        <page-footer />
    </div>
    `,
    data: function () {
        return {
            apiProductName: "",
            apiProductIndex: -1
        }
    },
    created () {
      this.apiProductName = this.$route.params.name
      for(i=0; i<this.$root.apiProducts.length; i++) {
        if (this.$root.apiProducts[i].name == this.apiProductName) {
          this.apiProductIndex = i;
        }
      }
      setTimeout(() => {
        this.load();
      }, 100);
    },
    beforeDestroy () {
        
    },
    methods: {
        load() {
            var specUrl = this.$root.apiProducts[this.apiProductIndex].specUrl;
            if (!specUrl)
              specUrl = "https://raw.githubusercontent.com/OAI/OpenAPI-Specification/fe795e257453908f463211c83b38a944daf1aa89/examples/v3.0/uber.yaml"; 
                       
            const ui = SwaggerUIBundle({
              url: specUrl,
              dom_id: '#swagger-ui'
            });
        },
        back() {
          router.go(-1);
        }
    }    
});