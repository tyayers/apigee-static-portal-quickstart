Vue.component('register', {
  template: `
    <div>
        <hr>
        <section>
            <form style="width: 100%; max-width: 400px;">
                <header>
                    <img src="https://upload.wikimedia.org/wikipedia/commons/a/aa/Apigee_logo.svg" height="60"/>
                </header>
                <div style="text-align: center">
                    <img src="../img/google-signin.png" style="cursor:pointer;" v-on:click="signInGoogle()">
                    <br><br>
                    <div> <i>or</i> </div>
                </div>                
                <div class="form-group">
                    <label for="register-firstname">First name</label>
                    <input type="text" v-model="firstName" class="form-control" id="register-firstname" placeholder="Enter first name">
                </div>
                <div class="form-group">
                    <label for="register-lastname">Last name</label>
                    <input type="text" v-model="lastName" class="form-control" id="register-firstName" placeholder="Enter last name">
                </div>                                
                <div class="form-group">
                    <label for="register-email">Email address</label>
                    <input type="email" v-model="email" class="form-control" id="register-email" aria-describedby="emailHelp" placeholder="Enter email">
                </div>
                <div class="form-group">
                    <label for="register-pw">Password</label>
                    <input type="password" v-model="pw" v-on:keypress="submitKeyDown($event)" class="form-control" id="register-pw" placeholder="Password">
                </div>
                
                <button id="register-submit" v-on:click="register($event)" ref="submitBtn" type="button" class="btn btn-primary">Register</button>
            </form>
        </section>
    </div>`,
    data: function() {
        return {
            firstName: "",
            lastName: "",
            email: "",
            pw: ""
        }
    },
    methods: {
        submitKeyDown: function(event) {
            if(event.keyCode==13) 
                this.register(event);
        },        
        register: function (event) {
            var origButton = this.$refs.submitBtn;
            origButton.innerHTML = "Working...";

            this.$root.temp.firstName = this.firstName;
            this.$root.temp.lastName = this.lastName;

            firebase.auth().createUserWithEmailAndPassword(this.email, this.pw).then(function() {
                if (app.user != undefined) {
                    console.log("sending email verification email..");
                    app.user.sendEmailVerification();
                }

                router.push({ name: 'sign-in', params: {sendConfirmationEmailOnLoad: true }});
            }).catch(function(error) {
                // Handle Errors here.
                var errorCode = error.code;
                var errorMessage = error.message;
                alert("There was an error with the registration. " + error.message + " " + error.code);
                origButton.innerHTML = "Register";
            });          
        },
        signInGoogle: function() {
            firebase.auth().signInWithPopup(googleProvider).then(function(result) {
                app.user = result.user;
                router.push("/api-products");
              }).catch(function(error) {
                // Handle Errors here.
                var errorCode = error.code;
                var errorMessage = error.message;
                // The email of the user's account used.
                var email = error.email;
                // The firebase.auth.AuthCredential type that was used.
                var credential = error.credential;
                // ...
                console.error("Google signin error.");
              });
        },
    }    
});