Vue.component('home', {
  template: `
    <div>
        <page-header />

        <header>
            <p>To get started, either</p>
            <p>
                <router-link to="/sign-in"><i>Sign In</i></router-link>
                <i>or</i>
                <router-link to="/register"><b>Register</b></router-link>
                <!--a download="mvp.html" href="./mvp.html" target="_blank"><i>Sign In ↗</i></a>
                <i>or</i>
                <a download="mvp.css" href="./mvp.css" target="_blank"><b>Register ↗</b></a-->
            </p>
            <br>
            <p>
                <sup>PRO TIP</sup>
                Register today and get €10.00 credited as a welcome bonus to your account!
            </p>
        </header>    

        <page-footer />
    </div>
    `,
    methods: {

    }
});