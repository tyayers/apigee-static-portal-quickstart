Vue.component('rateplans', {
  template: `
    <div>
        <page-header />
        <main>
           		
            <section id="api-products">
                <header>
                    <h3>Pricing & Plans</h3>
                </header>

                <div v-if="this.$root.rates.length == 0" class="card-loader card-loader--tabs" style="width: 280px; margin: 10px"></div>
                <div v-if="this.$root.rates.length == 0" class="card-loader card-loader--tabs" style="width: 280px; margin: 10px"></div>
                <div v-if="this.$root.rates.length == 0" class="card-loader card-loader--tabs" style="width: 280px; margin: 10px"></div>

                <aside v-for="(rate, index) in this.$root.rates" :key="rate.id" style="position: relative; height: 400px;">
                    <div>
                      <div style="width: 100%; text-align: center">
                        <img v-bind:src="rate.image" height="150">
                        <!--h3 style="margin-top: 5px">{{rate.displayName}}</h3-->
                      </div>
                        <p>
                            <h3>{{rate.name}}</h3>
                            <div v-for="(detail, detailIndex) in rate.ratePlanDetails" style="font-size: 14px">
                                <div v-for="(detailRate, rateIndex) in detail.ratePlanRates">
                                    {{detailRate.startUnit + "-" + detailRate.endUnit + " calls: " + rate.currency.name + " " + detailRate.rate + "/call"}}
                                </div>
                            </div>
                        </p>
                    </div>
                    <p v-if="rate.purchased" style="position: absolute; bottom: 5px">
                        <button type="button"  v-on:click="cancelRatePlan(rate.id)" class="btn btn-success btn-sm">Purchased</button>
                    </p>
                    <p v-if="!rate.purchased" style="position: absolute; bottom: 5px">
                        <button type="button" v-on:click="buyRatePlan(rate.id, $event)" class="btn btn-warning btn-sm click-subscribe" data-toggle="tooltip" data-placement="bottom" data-html="true" data-animation="true" title="Click to Subscribe">Purchase</button>
                    </p>
                </aside>                   
            </section>              
        </main>

        <page-footer />
    </div>
    `,
    data: function () {
        return {
            
        }
    },
    created () {
        if (!this.$root.user)
            router.push("/");
        else if (!this.$root.devProfile) {
            loadDevProfile(this.$root.user).then((devProfile) => {
                this.fetchData();
            });
        }
        else
            this.fetchData();
    },
    beforeDestroy () {

    },
    methods: {
        fetchData() {
            var user = this.$root.user;
            this.$root.user.getIdToken(false).then(function(token) {
                // First get developer's current apps
                var config = {
                    url: "/rateplans",
                    method: "get",
                    headers: {
                        Authorization: "Bearer " + token,
                        "Content-Type": "application/json"
                    }
                };

                axios(config).then(function (response) {
                    //console.log(response.data);
                    app.rates = response.data;

                    app.rates.forEach((item) => {
                        if (app.devProfile) {
                            for(i=0; i<app.devProfile.devRatePlans.length; i++) {
                                var devRate = app.devProfile.devRatePlans[i];
                                var now = new Date();
                                var devRateEndDate = new Date(devRate.endDate);
                                var devRateStartDate = new Date(devRate.startDate);
    
                                if (devRate.ratePlan.id == item.id && now > devRateStartDate && now < devRateEndDate) {
                                    item.purchased = true;
                                    break;
                                }
                                else {
                                    item.purchased = false;
                                }                                
                            }
                        }
                    });
                }).catch(function (error) {
                    console.log("get rates error " + JSON.stringify(error));
                });
            });
        },
        cancelRatePlan(id, index) {
            var user = this.$root.user;
            this.$root.user.getIdToken(false).then(function(token) {
                // First get developer's current apps
                var devRate;
                for(i=0; i<app.devProfile.devRatePlans.length; i++) {
                    var now = new Date();
                    var startDate = new Date(app.devProfile.devRatePlans[i].startDate);
                    var endDate = new Date(app.devProfile.devRatePlans[i].endDate);

                    if (now > startDate && now < endDate) {
                        devRate = app.devProfile.devRatePlans[i];
                        break;
                    }
                }
                
                if(devRate) {
                    var config = {
                        url: "/rateplans/" + id + "/" + devRate.id,
                        method: "delete",
                        headers: {
                            Authorization: "Bearer " + token,
                            "Content-Type": "application/json"
                        }
                    };
    
                    axios(config).then(function (response) {
                        console.log(response.data);
                    }).catch(function (error) {
                        console.log("get rates error " + JSON.stringify(error));
                    });
                }
            });        
        },
        buyRatePlan(id, index) {
            var user = this.$root.user;
            this.$root.user.getIdToken(false).then(function(token) {
                // First get developer's current apps
                var config = {
                    url: "/rateplans/" + id,
                    method: "post",
                    headers: {
                        Authorization: "Bearer " + token,
                        "Content-Type": "application/json"
                    }
                };

                axios(config).then(function (response) {

                    var rate = app.rates.find(x => x.id === id);
                    if (rate)
                        rate.purchased = true;
                    
                    if (app.devProfile) {
                        app.devProfile.devRatePlans.push(response.data)
                    }
                }).catch(function (error) {
                    console.error("buy rate plan error " + JSON.stringify(error));
                });
            });        }
    }
});