Vue.component('account', {
  template: `
    <div>
        <header>
            <page-navbar />
        </header>
        <main>
           	
            <article>
                <header v-if="this.$root.devProfile">
                    <h2>Hello, {{this.$root.devProfile.firstName}}</h2>
                </header>
                <header v-if="!this.$root.devProfile">
                    <h2>Hello</h2>
                </header>                

                <section v-if="!this.$root.devProfile" >
                <div class="card-loader card-loader--tabs" style="width: 280px; margin: 10px"></div>
                <div class="card-loader card-loader--tabs" style="width: 280px; margin: 10px"></div>
                <div class="card-loader card-loader--tabs" style="width: 280px; margin: 10px"></div>
                </section>
                <div v-if="this.$root.devProfile">
                    <h3>Your Account</h3>
                    <br>                
                    <h5>{{this.$root.devProfile.email}}</h5>
                    <br>
                    <h5>Balance</h5>
                    <div v-for="(account, index) in this.$root.devProfile.devCreditAccounts">
                        <span>{{account.id + ": " + (account.amount - account.usage).toFixed(2) + " " + account.currency + "  "}}</span><button class="btn btn-success btn-sm" v-on:click="addCredits(account.currency, 10)">Add credits</button>
                    </div>

                    <br>
                    <h5>Rate Plan Subscriptions</h5>
                    <div v-for="(rate, index) in this.$root.devProfile.devRatePlans">
                        <span>{{rate.ratePlan.id + ": " + rate.startDate + " - " + rate.endDate}}</span>
                    </div>    
                </div>            
               
            </article>              
        </main>

        <page-footer />
    </div>
    `,
    data: function () {
        return {
            
        }
    },
    created () {
        this.fetchData();
    },
    beforeDestroy () {

    },
    methods: {
        fetchData() {
            if (!this.$root.user)
                router.push("/");
            if (!this.$root.devProfile) {
                loadDevProfile(this.$root.user);
            }
        },
        addCredits(currency, amount) {
            var user = this.$root.user;
            this.$root.user.getIdToken(false).then(function(token) {
                // First get developer's current apps
                var config = {
                    url: "/rateplans/" + currency + "/credits",
                    method: "post",
                    headers: {
                        Authorization: "Bearer " + token,
                        "Content-Type": "application/json"
                    },
                    data: {
                        amount: amount,
                        currency: currency
                    }
                };

                axios(config).then(function (response) {
                    console.log(JSON.stringify(response.data, null, '\t'));

                    var account = app.devProfile.devCreditAccounts.find(x => x.id === response.data.id);
                    account.amount = response.data.amount;
                    account.usage = response.data.usage;
                    //app.rates = response.data;
                }).catch(function (error) {
                    console.log("get rates error " + JSON.stringify(error));
                });
            });        
        }
    }
});