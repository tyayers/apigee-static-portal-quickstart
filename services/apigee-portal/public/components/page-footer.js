Vue.component('page-footer', {
  template: `
    <footer>
        <section>Hosted in the ☁ with ❤ on <a href="https://cloud.google.com/run" target="_blank" style="margin-left: 5px;">Google Cloud Run</a></section>
    </footer>
    `,
    methods: {

    }
});