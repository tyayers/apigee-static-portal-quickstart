Vue.component('page-header', {
  template: `
    <header>
        <page-navbar />

        <h1>Apigee Static Portal <i>Quickstart</i></h1>
        <p>Test a simple static portal with <a href="https://cloud.google.com/apigee" target="_blank">Apigee</a> and <a href="https://firebase.google.com/docs/auth" target="_blank">Firebase Auth</a></p>
    </header>
    `,
    methods: {

    }
});