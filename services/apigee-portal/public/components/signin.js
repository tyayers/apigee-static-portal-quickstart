Vue.component('sign-in', {
  template: `
    <div>
        <hr>
        <section>
            <form style="width: 100%; max-width: 400px;">
                <header>
                    <img src="https://upload.wikimedia.org/wikipedia/commons/a/aa/Apigee_logo.svg" height="60"/>
                </header>
                <article v-if="this.$root.user!=undefined && !this.$root.user.emailVerified">
                    <aside>
                        <p>You are registered, but haven't completed your registration yet, please click on the confirm link in the mail you received.  Didn't get the mail?  <a href="#" v-on:click="sendConfirmationMail()">Click here to resend.</a></p>
                    </aside>
                </article>
                <div v-if="!this.$root.user" style="text-align: center">
                    <img src="../img/google-signin.png" style="cursor:pointer;" v-on:click="signInGoogle()">
                    <br><br>
                    <div> <i>or</i> </div>
                </div>
                <br>
                <div class="form-group">
                    <label for="signin-email">Email address</label>
                    <input type="email" v-model="email" class="form-control" id="signin-email" aria-describedby="emailHelp" placeholder="Enter email">
                </div>
                <div class="form-group">
                    <label for="signin-pw">Password</label>
                    <input type="password" v-model="pw" v-on:keypress="submitKeyDown($event)" class="form-control" id="signin-pw" placeholder="Password">
                </div>
                
                <button id="signin-submit" v-on:click="signIn()" ref="submitBtn" type="button" class="btn btn-primary">Sign in</button>
                <p>No account? <router-link to="/register">Click here to register.</router-link></p>
            </form>
        </section>
    </div>`,
    data: function() {
        return {
            email: "",
            pw: ""
        }
    },
    created () {
        if (this.sendConfirmationEmailOnLoad) {
            console.log("send confirmation flag set to true, sending email..");
            this.sendConfirmationMail();
        }
    },
    props: {
        sendConfirmationEmailOnLoad: Boolean
    },
    methods: {
        submitKeyDown: function(event) {
            if(event.keyCode==13) 
                this.signIn();
        },
        signIn: function () {
            var origButton = this.$refs.submitBtn;
            origButton.innerHTML = "Working...";
            
            firebase.auth().signInWithEmailAndPassword(this.email, this.pw).then(function() {
                if (app.user!=undefined && !app.user.emailVerified) {
                    alert("Please verify your email by clicking the link in the email you received to complete the registration.");
                    origButton.innerHTML = "Sign in";
                }
                else {
                    router.push("/api-products");
                }
            }).catch(function(error) {
                // Handle Errors here.
                var errorCode = error.code;
                var errorMessage = error.message;
                alert("There was an error with the signin. " + error.message + " " + error.code);
                origButton.innerHTML = "Sign in";
            });            
        },
        signInGoogle: function() {
            firebase.auth().signInWithPopup(googleProvider).then(function(result) {
                app.user = result.user;
                router.push("/api-products");
              }).catch(function(error) {
                // Handle Errors here.
                var errorCode = error.code;
                var errorMessage = error.message;
                // The email of the user's account used.
                var email = error.email;
                // The firebase.auth.AuthCredential type that was used.
                var credential = error.credential;
                // ...
                console.error("Google signin error.");
              });
        },
        sendConfirmationMail: function() {
            app.user.sendEmailVerification();
        }
    }
});