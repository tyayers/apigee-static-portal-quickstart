const routes = [
    { path: '/', name: "home", component: Vue.component("home") },    
    { path: '/sign-in', name: "sign-in", component: Vue.component("sign-in"), props: true },
    { path: '/register', component: Vue.component("register") },
    { path: '/api-products', name: "api-products", component: Vue.component("apiproducts")},
    { path: '/api-products/:name', component: Vue.component("apidoc")},
    { path: '/rateplans', name: "rateplans", component: Vue.component("rateplans")},
    { path: '/account', name: "account", component: Vue.component("account")}  
];

const router = new VueRouter({
  routes // short for `routes: routes`
});

var app = undefined;
var googleProvider = new firebase.auth.GoogleAuthProvider();

axios.defaults.headers.common["Content-Type"] = "application/json";
axios.defaults.baseURL = "";

// Initialize
var firebaseConfig = {
  apiKey: "AIzaSyD1HAISORL7R9JHC4qfAwurZpaK0Ynyg4o",
  authDomain: "apigee-portal.firebaseapp.com",
  databaseURL: "https://apigee-portal.firebaseio.com",
  projectId: "apigee-portal",
  storageBucket: "apigee-portal.appspot.com",
  messagingSenderId: "789103215233",
  appId: "1:789103215233:web:7b35a164b96e10d7fc4380",
  measurementId: "G-D0KF41EF8J"
};

firebase.initializeApp(firebaseConfig);
firebase.analytics();

// Firebase Auth State Changed

firebase.auth().onAuthStateChanged(function(user) {
    //console.log("user state change.");
    if (app == undefined) {
      app = new Vue({
        router: router,
        el: '#apigee-portal',
        data: {
          user: user,
          devProfile: undefined,
          apiProducts: [],
          apps: undefined,
          rates: [],                
          temp: {}
        }
      });
    }
    else
      app.user = user;

    //console.log(user);
    if (user && user.emailVerified) {
      if (router.currentRoute.name === "home") router.replace('/api-products');
      // loadDevProfile(user).then((devProfile) => {
      //   if (router.currentRoute.name === "home") router.replace('/api-products');         
      // })
    }
    else {
      if (user && !user.emailVerified) {
        user.getIdTokenResult(true).then(function(idTokenResult) {
          if (idTokenResult["claims"] != undefined && idTokenResult["claims"]["apigeeId"] == undefined) {
            // We don't yet have an apigee developer, so create now...
            var devEmail = idTokenResult["claims"]["email"];
  
            var firstName = app.temp.firstName;
            if (!firstName) firstName = devEmail.split('@')[0];
            var lastName = app.temp.lastName;
            if (!lastName) lastName = firstName;             
            var config = {
              url: "/developers",
              method: "post",
              headers: {
                  Authorization: "Bearer " + idTokenResult.token,
                  "Content-Type": "application/json"
              },
              data: {
                email: devEmail,
                firstName: firstName,
                lastName: lastName,
                userName: devEmail,
                firebaseId: user.uid
              }
            };
  
            axios(config).then(function (response) {}).catch(function (error) {
              console.error(error);
            });
          }
          else {
            // We have an apigee dev, so do nothing, user just has to confirm mail..
            console.log("user should verify their registration email..");
          }
        });
      }
      else {
        app.devProfile = undefined;
        app.apps = undefined;
        router.push("/");
      }        
    }
});

function getAppForProduct(apiProductName) {
	var result = undefined;
	//console.log("checking " + apiProductName);

	if (app.apps != undefined && app.apps.length > 0) {
		for(var i=0; i<app.apps.length; i++) {
			//console.log(apiProductName + " - " + app.apps[i].apiProduct);
			if (app.apps[i].apiProduct == apiProductName) {
				result = app.apps[i];
			}
		}
	}
	else {
		console.log("problem with apps " + JSON.stringify(app.apps, null, "\t"));
	}

	return result;
}

function loadDevProfile(user) {
  return new Promise((resolve, reject) => {
    user.getIdToken(false).then(function(token) {
      // First get developer's current apps
      var config = {
        url: "/developers/" + user.email,
        method: "get",
        headers: {
          Authorization: "Bearer " + token,
          "Content-Type": "application/json"
        }
      };

      axios(config).then(function (response) {
          //console.log(JSON.stringify(response.data, null, '\t'));
          //app.devProfile = response.data;
          app.devProfile = response.data;

          if ((!app.devProfile.devCreditAccounts || app.devProfile.devCreditAccounts.length == 0) && app.devProfile.currency) {
            // Give user introductory 10 credits
            var config = {
                url: "/rateplans/" + app.devProfile.currency + "/credits",
                method: "post",
                headers: {
                    Authorization: "Bearer " + token,
                    "Content-Type": "application/json"
                },
                data: {
                    amount: 10,
                    currency: app.devProfile.currency
                }
            };

            axios(config).then(function (response) {
                app.devProfile.devCreditAccounts = [
                  response.data
                ];
            }).catch(function (error) {
                console.log("get rates error " + JSON.stringify(error));
            });           
          }
          resolve(response.data);
          //router.go();
      }).catch(function (error) {
          console.log("load dev profile error " + JSON.stringify(error));
          reject();
      });
    }); 
  });
}